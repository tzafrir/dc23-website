---
name: Accommodation
---

# Accommodation

For accommodation we have partnered with Four Points by Sheraton Kochi Infopark situated inside Infopark, which is a 4 minutes walk from the venue. 
Attendees can avail accommodation [bursary](https://debconf23.debconf.org/about/bursaries/).

Hotel Address - Four Points by Sheraton Kochi Infopark, Kochi, Kerala - 682042 and [Map link](https://www.openstreetmap.org/node/9658370139#map=19/10.00774/76.36279).

Hotel has in-room Wi-Fi.

