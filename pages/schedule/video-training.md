---
name: Video Training
---

DebConf relies on volunteers to record and stream video. We cover all of
the scheduled content in the main 3 talk rooms, but this takes a lot of
people to make it happen.

Every room needs 6 video volunteers in each talk:

* 2x Camera Operators
* 1x Director (Video Mixer)
* 1x Audio Mixer
* 1x Talk Meister (to introduce the talks)
* 1x Room Coordinator

We need your help to do this work. If you're interested, please come to
a video training session in Lombardhi at 17:30. We'll teach you how to
do all of these tasks.

Video Team can be a great way to contribute to the conference, and
listen to talks at the same time.

Oh, and we have T-Shirts for volunteers who have completed 3 tasks 😄
